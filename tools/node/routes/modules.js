//
//  GET /modules
//
exports.index = function (req, res, next) {
    var allModules = createFileList("core/modules");
    var activeModules = getActiveModules();
    modules = createModuleList(allModules, activeModules);

    res.render('modules', { title: "Modules", modules: modules, activeModules: activeModules});
};
