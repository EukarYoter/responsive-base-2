"use strict";

var app = {
    name: "insert-customer-name-here",
    version: "0.1.0",
    required: {
        "J": "0.8.5"
    },
    config: {
    },
    init: function () {
        // Add custom functions to each page type,
        // Please: do not remove the J.component.* functions

        J.pages.addToQueue("all-pages", function () {
            J.components.getManufacturerList();
            J.components.footer();
            J.components.pagingControll();
        });

        J.pages.addToQueue("start-page", function () {
            J.components.startPageObjects();
        });

        J.pages.addToQueue("product-page", function () {
            J.components.tabSystem();
            J.components.productThumbnails();
            J.components.leftCategoryMenu();
            J.components.stockNotification();
        });

        J.pages.addToQueue("category-advanced-page", function () {
            J.components.categoryWrapper();
            J.components.categoryDescription();
            J.components.leftCategoryMenu();
        });

        J.pages.addToQueue("category-page", function () {
            J.components.categoryWrapper();
            J.components.categoryDescription();
            J.components.leftCategoryMenu();
        });

        J.pages.addToQueue("orderconfirmation-page", function () {
            J.components.orderConfirm();
        });

        J.pages.addToQueue("manufacturer-advanced-page", function () {
            J.components.categoryWrapper();
            J.components.categoryDescription();
            J.components.leftCategoryMenu();
        });

        J.pages.addToQueue("manufacturer-page", function () {
            J.components.categoryWrapper();
            J.components.categoryDescription();
            J.components.leftCategoryMenu();
        });

        J.pages.addToQueue("news-page", function () {

        });

        J.pages.addToQueue("checkout-page", function () {

        });

        J.pages.addToQueue("searchresult-page", function () {
            $("table.search-result-table td:nth-child(3)").addClass("search-article-number");
        });

        J.pages.addToQueue("my-page", function () {

        });

        J.pages.addToQueue("changepassword-page", function () {

        });

        J.pages.addToQueue("standard-page", function () {

        });

        J.pages.addToQueue("sitemap-page", function () {

        });
    }
};

J.pages.addToQueue("all-pages", app.init);