var inlineProduct = {
    version: "0.2.1",
    init: function () {
        J.translations.push(
            {
                buyButton: {
                    sv: "Köp",
                    en: "Buy",
                    da: "Köp",
                    nb: "Köp",
                    fi: "Köp"
                }
            },
            {
                infoButton: {
                    sv: "Läs mer",
                    en: "Read more",
                    da: "Läs mer",
                    nb: "Läs mer",
                    fi: "Läs mer"
                }
            }
        );
        inlineProduct.createList();
        inlineProduct.attachIdentifiers();
    },
    productList: [],
    isVisible : function () {
        return ($("#inline-product-placeholder").is(":visible"));
    },
    currentId : function () {
        var id = $("#inline-product-placeholder").find(".product-expanded-outer-wrapper").attr("product-id");
        if (typeof(id) === "undefined"){
            return false;
        }
        return id;
    },
    callbackObject: {
        success: function(data, callbackOptions, textStatus, jqXHR){
            inlineProduct.renderProduct(data, callbackOptions);
        }
    },
    createList: function () {
        // data-productid is found in .product-name > h3 > a
        $("ul").find("[data-productid]").each(function () {
            inlineProduct.productList.push(parseInt($(this).attr("data-productid")));
        });
    },
    attachIdentifiers: function () {
        $(".category-page-wrapper > ul li").each(function (index, element) {
            var id = inlineProduct.productList[index];
            $(element).addClass("product-item");
            $(element).attr("product-id", id);
            $(element).find(".button-info").attr("onClick", "inlineProduct.toggleProduct(" + id + "); return false;");
        });
    },
    toggleProduct: function (id) {
        if (inlineProduct.isVisible()) {
            if (inlineProduct.currentId() == id) {
                inlineProduct.close();
            } else {
                J.api.product.get(inlineProduct.callbackObject, id, true, 1, id);
            }
        } else {
            J.api.product.get(inlineProduct.callbackObject, id, true, 1, id);
        }
    },
    findLastInRow: function (id) {
        var rows = getRows('.category-page-wrapper > ul li');
        for (i = 0; i < rows.length; i++) {
            for (j = 0; j < rows[i].length; j++) {
                var temp = $(rows[i][j]);
                if (temp.attr("product-id") == id) {
                    return ($(rows[i][rows[i].length -1]).attr("product-id"));
                }
            }
        }
        return false;
    },
    attachProductPlaceholder: function (id) {
        var lastItemInRow = inlineProduct.findLastInRow(id);
        var nth = inlineProduct.productList.indexOf(parseInt(lastItemInRow)) + 1;
        $(".category-page-wrapper ul li:nth-child(" + nth + ")").after('<div id="inline-product-placeholder"></div>');
    },
    renderProduct: function (data, callbackOptions) {
        data = data.ProductItems[0];
        data.productId = callbackOptions;
        //log(data);
        inlineProduct.attachProductPlaceholder(callbackOptions);
        var template = J.views['inline-product/inline-product'];
        var html = template(data);
        $("#inline-product-placeholder").html("").append(html);
    },
    close: function () {
        $("#inline-product-placeholder").remove();
    },
    onResize: function () {
        if (inlineProduct.isVisible()) {
            var id = inlineProduct.currentId();
            inlineProduct.close();
            inlineProduct.toggleProduct(id);
        } else {

        }
    }
};
J.pages.addToQueue("category-page", inlineProduct.init);
$(window).resize(function () {
    inlineProduct.onResize();
});

