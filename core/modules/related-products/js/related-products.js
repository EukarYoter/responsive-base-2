var relatedProducts = {
    init: function () {
        if (J.data.productId) {
            J.translations.push(
                {
                    buyButton: {
                        sv: "Köp",
                        en: "Buy",
                        da: "Köp",
                        nb: "Köp",
                        fi: "Köp"
                    }
                },
                {
                    infoButton: {
                        sv: "Läs mer",
                        en: "Read more",
                        da: "Läs mer",
                        nb: "Läs mer",
                        fi: "Läs mer"
                    }
                }
            );
            J.api.product.getRelatedProducts(relatedProducts.render, J.data.productId, true, 5, J.data.productId)
        }
    },
    render: function (data, callbackOptions) {
        var template = J.views['related-products/related-products'];
        var html = template(data);
        $(".product-toolbar").after("<div id='list-category-placeholder'></div>");
        $("#list-category-placeholder").append(html);
    }
};

J.pages.addToQueue("product-page", relatedProducts.init);
