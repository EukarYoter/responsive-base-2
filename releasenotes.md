## Jetshop Responsive Base Framework
# Releasenotes 

##v1.0.0

- Major change: client-specific development are now done in file client.js
- Renamed 'responsive-base.js' to 'responsive-base-core.js.' This file is now treated as a library
- Major changes to folder structure: folder 'core' now contains files that build to folder 'stage'
- Added 3 modules to handle category-navigation on desktop
- Added module 'mobile-cat-nav' for category-navigation on smaller screens
- Replaced old module 'add-to-cart-popup' with module 'dynamic-cart'
- J.cart now contains way more information on cart content
- Grunt-tasks for uploading are now separated to speed up development
- Renamed 'templates' to 'pages' for consistency, and to avoid confusion with Handlebars-views
- Handlebars-views now use file extension .hbs for better compatibility with editors
- Replaced Foundation.utils.helper with internal logic for switch detection
- Added JS-events for cart-loaded, cart-updated and device-width switch
- J.api now accepts objects as callback, honoring AJAX-methods before, error, success & complete
- J.api.product.get now accepts int as well as array
- Added 'getRelatedProducts' to  J.api.product 
- Added startpages list & get to J.api
- Resolved bug with J.api & quota-exceeded errors
- Resolved bug with Android 4.3 rendering white pages
- Resolved bug with responsive order confirm being wrongfully identified as "checkout-page"
- Resolved bug in setCookie to make it possible to set session cookies without specifying expiry time.
- Resolved bug with disappearing myPages-link
- Prevent J.init() from firing twice, resolves issue #32.
- Added Node server for listing of module, as well as debugging
- Updated Font Awesome to version 4.5

##v0.8.6
- Minor bugfixes: #29, #32, #31, #25

##v0.8.5
- Project renamed to "Responsive Base"
- Added J.api to get an uniform interface for fetching REST-data
- Added optional caching of REST-data to Local Storage
- Added JS templating engine "Handlebars". Templates are exposed as J.views  
- Added several helpers for Handlebars to handle JetshopData (translate, currentLanguage, currentCulture)
- Added jquery-match-height to replace setSameHeight() functionality
- Added grunt-focus to enable targets in grunt-contrib-watch

##v0.8.4
- Refactored handling of JetshopData & jlib. J.checker are now booleans only, all other Jetshop-specific data are moved into J.data 

##v0.8.3
- Major changes to handling of pageType functionality, all initPageTypeXX functions are now gone and replaced by J.pages 
- J.pages accept queues: ``J.pages.addToQueue("page-default",myCustomFunction);`` This will make modules more independent

##v0.8.2
- All functions per pageType that previously were private are now bundled in J.components 
- Major changes to functions that are triggered by changes to device width. J.initSwitchToSmall are gone and replaced by J.switch. J.switch accepts queues: ``J.switch.addToSmall(checkMobileMenuStyles);``
- Major changes to handling of translations, J.translations are now an array that accept push: ;``J.translations.push({ translationsObjects});;`` 
- Added ``grunt noftp`` which is default tasks minus the FTP-handling. This will speed up development time when working locally (or from Brazil)
- Resolved issues caused by conflicting class-names with Foundation

##v0.8.1
- Resolved bug in initTabSystem()
- Resolved bug in startPageObjects()
- Resolved bug with input heights on iOS
- Major restructuring of SCSS
- Updated readme.md

##v0.8.0
- Added modules
- Added Universal Grid: layout, css and DOM are now identical for listProducts, startPageObjects and Releware
- Moved J.init() from template to script-file
- Added support for support.css, this file will not be handled by FTP deploy
- Resolved bug in J.setPageType regarding pages with productId & categoryId set to 0
- Numerous bugfixes and issues resolved
- Added functionality in responsive-base-libraries.js
- Added ``grunt package``
- Major update to readme's, documentation and support sites

##v0.7.0
- Added templates for Releware
- Added functionality to Gruntfile
- Added the entire Foundation SCSS package
- Bugfixes for iOS

